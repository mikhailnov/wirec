#!/usr/bin/env bash

# Пример: 
# есть страница http://www.zavuch.ru/news/announces/1050/
# В коде страницы находим src iframe, это http://rep0.zavuch.ru/nagoi/events/view_event/11092
# Это и будет ссылка для записи экранного видео

wirec_template_run(){
	
	if [ "$FULLSCREEN" = '1' ]
		then
			xdotool key F11
			sleep 2
			xdotool mousemove 385 704 # for fullscreen chromium
		else
			xdotool mousemove 385 704
	fi
	xdotool click 1
	sleep 1
}
