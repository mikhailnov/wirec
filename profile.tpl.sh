#!/usr/bin/env bash
# WIREC PROFILE TEMPLATE

# в профиле ТОЛЬКО ЗАДАВАТЬ ПЕРЕМЕННЫЕ, а все действия вынести в функции, которые не вызываются при source!
# затем уже из основного скрипта вызовем эти функции со стандартными именами

wirec_template_run(){
	# https://askubuntu.com/questions/267916/how-can-i-get-chromium-browser-to-run-fullscreen-not-kiosk-in-12-04
	
	if [ "$FULLSCREEN" = '1' ]
		then
			xdotool key F11
			sleep 2
	fi
	width="$(echo "$RESOLUTION" | awk -F 'x' '{print $1}')"
	height="$(echo "$RESOLUTION" | awk -F 'x' '{print $2}')"
	xdotool mousemove $((width / 2)) $((height / 2)) click 1
	sleep 1
}
