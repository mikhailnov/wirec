#
PREFIX = /
TMPHOME=/tmp/wirec-build

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/usr/bin
	install -d $(DESTDIR)/$(PREFIX)/usr/share/wirec
	install -d $(DESTDIR)/$(PREFIX)/usr/share/icons
	install -d $(DESTDIR)/$(PREFIX)/usr/share/applications
	install -m0755 ./wirec.sh $(DESTDIR)/$(PREFIX)/usr/bin/wirec
	install -m0644 ./profile.tpl.sh $(DESTDIR)/$(PREFIX)/usr/share/wirec/profile.tpl.sh
	install -m0644 ./desktop/wirec.desktop $(DESTDIR)/$(PREFIX)/usr/share/applications/wirec.desktop
	
	pkill libreoffice || true
	pkill -9 libreoffice || true
	pkill soffice || true
	pkill -9 soffice || true
	mkdir -p $(TMPHOME)
	env HOME=$(TMPHOME) libreoffice --headless --convert-to svg --outdir $(DESTDIR)/$(PREFIX)/usr/share/icons desktop/wirec.odg
	
uninstall:
	rm -fv $(PREFIX)/usr/bin/wirec
	rm -fv $(PREFIX)/usr/share/icons/wirec.svg
	rm -fv $(PREFIX)/usr/share/applications/wirec.desktop
	rm -fvr $(PREFIX)/usr/share/wirec/
